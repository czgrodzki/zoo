package pl.edu.pg.zoo.owners.service;

import org.junit.jupiter.api.Test;
import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.cat.storage.CatsRepositoryImpl;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;
import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.dog.storage.DogsRepositoryImpl;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;
import pl.edu.pg.zoo.animals.turtles.storage.TurtlesRepositoryImpl;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;
import pl.edu.pg.zoo.owners.model.Owner;
import pl.edu.pg.zoo.owners.service.port.OwnerService;
import pl.edu.pg.zoo.owners.storage.OwnersRepositoryImpl;
import pl.edu.pg.zoo.owners.storage.port.OwnersRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static pl.edu.pg.zoo.animals.helpers.Gender.MALE;

class OwnerServiceImplTest {

    private final OwnersRepository ownersRepository = new OwnersRepositoryImpl();
    private final CatsRepository catsRepository = new CatsRepositoryImpl();
    private final DogsRepository dogsRepository = new DogsRepositoryImpl();
    private final TurtlesRepository turtlesRepository = new TurtlesRepositoryImpl();
    private final OwnerService ownerService = new OwnerServiceImpl(ownersRepository, catsRepository, dogsRepository, turtlesRepository);

    @Test
    void addingOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));

        // when
        ownerService.addOwner(owner);
        ownerService.addOwner(owner2);

        // then
        var owners = ownersRepository.findOwners();
        assertThat(owners, containsInAnyOrder(owner, owner2));

    }

    @Test
    void addingOwnerWithWrongDateTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(1800, 12, 12));

        // when
        var ownerResponse = ownerService.addOwner(owner);

        // then
        assertFalse(ownerResponse.isSuccess());

    }

    @Test
    void removeOwnerHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));

        // when
        ownerService.addOwner(owner);
        ownerService.addOwner(owner2);
        ownerService.deleteOwnerById(owner.getId());

        // then
        var owners = ownerService.findOwners();
        assertEquals(1, owners.size());

    }

    @Test
    void removeOwnerWhenNoOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));

        // when
        ownerService.addOwner(owner);
        ownerService.addOwner(owner2);
        ownerService.deleteOwnerById(100);

        // then
        var owners = ownerService.findOwners();
        assertEquals(2, owners.size());

    }

    @Test
    void findOwnerByIdHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));

        // when
        ownerService.addOwner(owner);
        ownerService.addOwner(owner2);

        // then
        var ownerById = ownerService.findOwnerById(owner2.getId());
        assertEquals(ownerById.getRight().getId(), owner2.getId());

    }

    @Test
    void findOwnerByIdNoOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));

        // when
        ownerService.addOwner(owner);
        ownerService.addOwner(owner2);

        // then
        var ownerById = ownerService.findOwnerById(199);
        assertFalse(ownerById.isSuccess());

    }

    @Test
    void updateOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        String updatedName = "Jan";
        String updatedSurname = "Janowski";

        // when
        ownerService.addOwner(owner);
        ownerService.updateOwner(owner.getId(), updatedName, updatedSurname);

        // then
        var ownerById = ownerService.findOwnerById(owner.getId());
        assertEquals(updatedName, ownerById.getRight().getName());
        assertEquals(updatedSurname, ownerById.getRight().getSurname());

    }

    @Test
    void groupAnimalsByOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner3 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner4 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));

        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner4.getId());
        Tortoise tortoise2 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner3.getId());
        Tortoise tortoise3 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner2.getId());
        Tortoise tortoise4 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());

        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner3.getId(), "buras");
        Dog dog2 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner3.getId(), "buras");
        Dog dog3= new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner2.getId(), "buras");
        Dog dog4 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner4.getId(), "buras");

        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner2.getId(), "buras");
        Cat cat2 = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner2.getId(), "buras");
        Cat cat3 = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner4.getId(), "buras");
        Cat cat4 = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner4.getId(), "buras");

        // when
        ownerService.addOwner(owner);
        ownerService.addOwner(owner2);
        ownerService.addOwner(owner3);
        ownerService.addOwner(owner4);

        turtlesRepository.addTortoise(tortoise);
        turtlesRepository.addTortoise(tortoise2);
        turtlesRepository.addTortoise(tortoise3);
        turtlesRepository.addTortoise(tortoise4);

        dogsRepository.addDog(dog);
        dogsRepository.addDog(dog2);
        dogsRepository.addDog(dog3);
        dogsRepository.addDog(dog4);

        catsRepository.addCat(cat);
        catsRepository.addCat(cat2);
        catsRepository.addCat(cat3);
        catsRepository.addCat(cat4);

        // then

        Map<Owner, List<Animal>> ownersWithTheirAnimals = ownerService.getOwnersWithTheirAnimals();
        assertThat(ownersWithTheirAnimals.get(owner), containsInAnyOrder(tortoise4));
        assertThat(ownersWithTheirAnimals.get(owner2), containsInAnyOrder(tortoise3, dog3, cat, cat2));
        assertThat(ownersWithTheirAnimals.get(owner3), containsInAnyOrder(tortoise2, dog, dog2));
        assertThat(ownersWithTheirAnimals.get(owner4), containsInAnyOrder(tortoise, dog4, cat3, cat4));

    }

    @Test
    void savingAndLoadingTest() {
        // given
        Owner owner = new Owner("Jan", "Dupa", MALE, LocalDate.now());
        Owner owner2 = new Owner("Jan", "Dupa", MALE, LocalDate.now());
        Owner owner3 = new Owner("Jan", "Dupa", MALE, LocalDate.now());

        // when
        ownerService.addOwner(owner);
        ownerService.addOwner(owner2);
        ownerService.addOwner(owner3);

        // Dumping data to file and removing all from app to check if owners will appear
        ownerService.saveData();

        ownerService.deleteOwnerById(owner.getId());
        ownerService.deleteOwnerById(owner2.getId());
        ownerService.deleteOwnerById(owner3.getId());
        assertTrue(ownerService.findOwners().isEmpty());

        // then
        ownerService.loadData();
        var owners = ownerService.findOwners();

        assertThat(owners, containsInAnyOrder(owner, owner2, owner3));


    }
}
