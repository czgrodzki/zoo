package pl.edu.pg.zoo.animals.turtles.model;

import org.junit.jupiter.api.Test;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.move.MoveDirection;
import pl.edu.pg.zoo.move.MoveResponse;
import pl.edu.pg.zoo.owners.model.Owner;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class TortoiseTest {

    @Test
    void moveTortoiseHappyPath() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolwik", Gender.MALE, LocalDate.of(1990, 1, 1), owner.getId());

        // when
        tortoise.move(MoveDirection.RIGHT);

        // then
        assertEquals(2, tortoise.getPosition().getColumn());
        assertEquals(1, tortoise.getPosition().getRow());
        assertFalse(tortoise.isActive());

    }

    @Test
    void cannotMoveBecauseNotActiveAfterMoveTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolwik", Gender.MALE, LocalDate.of(1990, 1, 1), owner.getId());

        // when
        tortoise.move(MoveDirection.RIGHT);
        MoveResponse moveResponse = tortoise.move(MoveDirection.RIGHT);

        // then
        assertFalse(moveResponse.isSuccess());
        assertEquals("Tortoise is hidden in shell", moveResponse.getLeft());
        assertNull(moveResponse.getRight());

    }

}