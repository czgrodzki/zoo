package pl.edu.pg.zoo.animals.cat.service;

import org.junit.jupiter.api.Test;
import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.cat.service.portal.CatsService;
import pl.edu.pg.zoo.animals.cat.storage.CatsRepositoryImpl;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;
import pl.edu.pg.zoo.animals.commons.AnimalResponse;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.owners.model.Owner;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class CatsServiceImplTest {

    private final CatsRepository catsRepository = new CatsRepositoryImpl();
    private final CatsService catsService = new CatsServiceImpl(catsRepository);

    @Test
    void addingCatTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Cat cat2 = new Cat("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Cat cat3 = new Cat("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        catsService.addCat(cat);
        catsService.addCat(cat2);
        catsService.addCat(cat3);

        // then
        var cats = catsService.findCats();
        assertThat(cats, containsInAnyOrder(cat, cat2, cat3));

    }

    @Test
    void addingCatWithWrongDateTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2030, 1, 1), owner.getId(), "buras");

        // when
        AnimalResponse animalResponse = catsService.addCat(cat);

        // then
        assertFalse(animalResponse.isSuccess());

    }

    @Test
    void removeCatHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner.getId(), "buras");
        Cat cat2 = new Cat("Kicius", Gender.MALE, LocalDate.of(2020, 1, 1), owner.getId(), "buras");

        // when
        catsService.addCat(cat);
        catsService.addCat(cat2);
        catsService.deleteCatById(cat.getId());

        // then
        var cats = catsService.findCats();
        assertEquals(1, cats.size());

    }

    @Test
    void removeCatWhenNoCatTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner.getId(), "buras");
        Cat cat2 = new Cat("Cycus", Gender.MALE, LocalDate.of(2005, 1, 1), owner.getId(), "buras");

        // when
        catsService.addCat(cat);
        catsService.addCat(cat2);
        catsService.deleteCatById(1000L);

        // then
        var cats2 = catsService.findCats();
        assertEquals(2, cats2.size());

    }

    @Test
    void findCatByIdHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner.getId(), "buras");
        Cat cat2 = new Cat("Kicius", Gender.MALE, LocalDate.of(2020, 1, 1), owner.getId(), "buras");

        // when
        catsService.addCat(cat);
        catsService.addCat(cat2);

        // then
        var catById = catsService.findCatById(cat2.getId());
        assertEquals(catById.getRight().getId(), cat2.getId());

    }

    @Test
    void findCatByIdNoCatTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner.getId(), "buras");
        Cat cat2 = new Cat("Kicius", Gender.MALE, LocalDate.of(2020, 1, 1), owner.getId(), "buras");

        // when
        catsService.addCat(cat);
        catsService.addCat(cat2);

        // then
        var catById = catsService.findCatById(199);
        assertFalse(catById.isSuccess());

    }

    @Test
    void updateOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Jan", "Jan", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner.getId(), "buras");

        // when
        catsService.addCat(cat);
        catsService.updateCatsOwner(cat.getId(), owner2.getId());

        // then
        var catById = catsService.findCatById(cat.getId());
        assertEquals(owner2.getId(), catById.getRight().getOwnerId());

    }

    @Test
    void updateOwnerWhenNoCatTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Jan", "Jan", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2010, 1, 1), owner.getId(), "buras");

        // when
        catsService.addCat(cat);
        AnimalResponse animalResponse = catsService.updateCatsOwner(200L, owner2.getId());

        // then
        assertFalse(animalResponse.isSuccess());

    }

}