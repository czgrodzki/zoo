package pl.edu.pg.zoo.animals;

import org.junit.jupiter.api.Test;
import pl.edu.pg.zoo.Constant;
import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.move.MoveDirection;
import pl.edu.pg.zoo.move.MoveResponse;
import pl.edu.pg.zoo.owners.model.Owner;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

class AnimalTest {

    @Test
    void moveHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        //when
        cat.move(MoveDirection.UP);
        cat.move(MoveDirection.DOWN);
        cat.move(MoveDirection.DOWN);
        cat.move(MoveDirection.LEFT);
        cat.move(MoveDirection.RIGHT);

        // then
        assertEquals(3,cat.getPosition().getRow());
        assertEquals(2,cat.getPosition().getColumn());

    }

    @Test
    void cannotMoveUpTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat( "Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        //when
        MoveResponse move = cat.move(MoveDirection.UP);

        // then
        assertFalse(move.isSuccess());
        assertEquals("Animal is standing next to the fence", move.getLeft());

    }

    @Test
    void cannotMoveLeftTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        //when
        MoveResponse move = cat.move(MoveDirection.LEFT);

        // then
        assertFalse(move.isSuccess());
        assertEquals("Animal is standing next to the fence", move.getLeft());

    }

    @Test
    void cannotMoveRightTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        //when
        cat.getPosition().setColumn(Constant.ZOO_SIZE);
        MoveResponse move = cat.move(MoveDirection.RIGHT);

        // then
        assertFalse(move.isSuccess());
        assertEquals("Animal is standing next to the fence", move.getLeft());

    }

    @Test
    void cannotMoveDownTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Cat cat = new Cat("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        //when
        cat.getPosition().setRow(Constant.ZOO_SIZE);
        MoveResponse move = cat.move(MoveDirection.DOWN);

        // then
        assertFalse(move.isSuccess());
        assertEquals("Animal is standing next to the fence", move.getLeft());

    }


}