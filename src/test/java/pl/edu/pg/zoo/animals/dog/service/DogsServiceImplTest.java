package pl.edu.pg.zoo.animals.dog.service;

import org.junit.jupiter.api.Test;
import pl.edu.pg.zoo.animals.commons.AnimalResponse;
import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.dog.service.portal.DogsService;
import pl.edu.pg.zoo.animals.dog.storage.DogsRepositoryImpl;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.owners.model.Owner;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class DogsServiceImplTest {

    private final DogsRepository repository = new DogsRepositoryImpl();
    private final DogsService service = new DogsServiceImpl(repository);

    @Test
    void addingDogTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Dog dog2 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Dog dog3 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        service.addDog(dog);
        service.addDog(dog2);
        service.addDog(dog3);
        // then
        var dogs = service.findDogs();
        assertThat(dogs, containsInAnyOrder(dog, dog2, dog3));

    }

    @Test
    void addingDogWithWrongDateTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(1700, 1, 1), owner.getId(), "buras");

        // when
        AnimalResponse animalResponse = service.addDog(dog);

        // then
        assertFalse(animalResponse.isSuccess());

    }

    @Test
    void removeDogHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Dog dog2 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        service.addDog(dog);
        service.addDog(dog2);
        service.deleteDogById(dog2.getId());

        // then
        var dogs = service.findDogs();
        assertEquals(1, dogs.size());

    }

    @Test
    void removeDogWhenNoDogTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Dog dog2 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        service.addDog(dog);
        service.addDog(dog2);
        service.deleteDogById(1000L);

        // then
        var dogs = service.findDogs();
        assertEquals(2, dogs.size());

    }

    @Test
    void findDogByIdHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Dog dog2 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        service.addDog(dog);
        service.addDog(dog2);

        // then
        var dogById = service.findDogById(dog2.getId());
        assertEquals(dogById.getRight().getId(), dog2.getId());

    }

    @Test
    void findDogByIdNoDogTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");
        Dog dog2 = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        service.addDog(dog);
        service.addDog(dog2);

        // then
        var dogById = service.findDogById(1000L);
        assertFalse(dogById.isSuccess());

    }

    @Test
    void updateOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        service.addDog(dog);
        service.updateDogsOwner(dog.getId(), owner2.getId());

        // then
        var dogById = service.findDogById(dog.getId());
        assertEquals(owner2.getId(), dogById.getRight().getOwnerId());

    }

    @Test
    void updateOwnerWhenNoDogTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Dog dog = new Dog("Kicius", Gender.MALE, LocalDate.of(2019, 1, 1), owner.getId(), "buras");

        // when
        service.addDog(dog);
        AnimalResponse animalResponse = service.updateDogsOwner(1000L, owner2.getId());

        // then
        assertFalse(animalResponse.isSuccess());

    }

}