package pl.edu.pg.zoo.animals.turtles.service;

import org.junit.jupiter.api.Test;
import pl.edu.pg.zoo.animals.commons.AnimalResponse;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;
import pl.edu.pg.zoo.animals.turtles.service.portal.TurtlesService;
import pl.edu.pg.zoo.animals.turtles.storage.TurtlesRepositoryImpl;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;
import pl.edu.pg.zoo.owners.model.Owner;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

class TurtlesServiceImplTest {

    private final TurtlesRepository repository = new TurtlesRepositoryImpl();
    private final TurtlesService service = new TurtlesServiceImpl(repository);

    @Test
    void addingTortoiseTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());
        Tortoise tortoise2 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());
        Tortoise tortoise3 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());


        // when
        service.addTortoise(tortoise);
        service.addTortoise(tortoise2);
        service.addTortoise(tortoise3);

        // then
        var turtles = service.findTurtles();
        assertThat(turtles, containsInAnyOrder(tortoise, tortoise2, tortoise3));
    }

    @Test
    void addingTortoiseWithWrongDateTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1850, 1, 1), owner.getId());

        // when
        var tortoiseRespond = service.addTortoise(tortoise);

        // then
        assertFalse(tortoiseRespond.isSuccess());

    }

    @Test
    void removeTortoiseHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());
        Tortoise tortoise2 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());

        // when
        service.addTortoise(tortoise);
        service.addTortoise(tortoise2);
        service.deleteTortoiseById(tortoise.getId());

        // then
        var cats = service.findTurtles();
        assertEquals(1, cats.size());

    }

    @Test
    void removeTortoiseWhenNoCatTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());
        Tortoise tortoise2 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());

        // when
        service.addTortoise(tortoise);
        service.addTortoise(tortoise2);
        service.deleteTortoiseById(1000L);

        // then
        var turtles = service.findTurtles();
        assertEquals(2, turtles.size());

    }

    @Test
    void findTortoiseByIdHappyPathTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());
        Tortoise tortoise2 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());

        // when
        service.addTortoise(tortoise);
        service.addTortoise(tortoise2);

        // then
        var tortoiseById = service.findTortoiseById(tortoise2.getId());
        assertEquals(tortoise2.getId(), tortoiseById.getRight().getId());

    }

    @Test
    void findTortoiseByIdNoTortoiseTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());
        Tortoise tortoise2 = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());

        // when
        service.addTortoise(tortoise);
        service.addTortoise(tortoise2);

        // then
        var tortoiseById = service.findTortoiseById(1000L);
        assertFalse(tortoiseById.isSuccess());
    }

    @Test
    void updateOwnerTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());

        // when
        service.addTortoise(tortoise);
        service.updateTortoisesOwner(tortoise.getId(), owner2.getId());

        // then
        var tortoiseById = repository.findTortoiseById(tortoise.getId());
        assertEquals(owner2.getId(), tortoiseById.get().getOwnerId());

    }

    @Test
    void updateOwnerWhenNoCatTest() {
        // given
        Owner owner = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Owner owner2 = new Owner("Roman", "Roman", Gender.MALE, LocalDate.of(2000, 12, 12));
        Tortoise tortoise = new Tortoise("Zolw", Gender.FEMALE, LocalDate.of(1950, 1, 1), owner.getId());

        // when
        service.addTortoise(tortoise);
        var tortoiseRespond = service.updateTortoisesOwner(1000L, owner2.getId());

        // then
        assertFalse(tortoiseRespond.isSuccess());

    }


}