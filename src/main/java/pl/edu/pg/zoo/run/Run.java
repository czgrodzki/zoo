package pl.edu.pg.zoo.run;

import pl.edu.pg.zoo.animals.animal.service.AnimalServiceImpl;
import pl.edu.pg.zoo.animals.animal.service.portal.AnimalService;
import pl.edu.pg.zoo.animals.cat.storage.CatsRepositoryImpl;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;
import pl.edu.pg.zoo.animals.dog.storage.DogsRepositoryImpl;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;
import pl.edu.pg.zoo.animals.presentation.PresentationImpl;
import pl.edu.pg.zoo.animals.turtles.storage.TurtlesRepositoryImpl;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;
import pl.edu.pg.zoo.owners.service.OwnerServiceImpl;
import pl.edu.pg.zoo.owners.service.port.OwnerService;
import pl.edu.pg.zoo.owners.storage.OwnersRepositoryImpl;
import pl.edu.pg.zoo.owners.storage.port.OwnersRepository;

import java.util.Scanner;

public class Run {

    PresentationImpl presentation = new PresentationImpl();
    CatsRepository catsRepository = new CatsRepositoryImpl();
    DogsRepository dogsRepository = new DogsRepositoryImpl();
    TurtlesRepository turtlesRepository = new TurtlesRepositoryImpl();
    OwnersRepository ownersRepository = new OwnersRepositoryImpl();
    OwnerService ownerService = new OwnerServiceImpl(ownersRepository, catsRepository, dogsRepository, turtlesRepository);
    AnimalService animalService = new AnimalServiceImpl(catsRepository, dogsRepository, turtlesRepository);
    Scanner scanner = new Scanner(System.in);


    public void run() {
        presentation.loadData();
        printMenu();
        System.out.println();
        int task = scanner.nextInt();
        while (task != 11) {
            switch (task) {
                case 1:
                    presentation.addOwner();
                    break;
                case 2:
                    presentation.deleteOwner();
                    break;
                case 3:
                    presentation.showSingleOwnerAnimals();
                    break;
                case 4:
                    presentation.showAllOwnersAndAnimals();
                    break;
                case 5:
                    presentation.addAnimal();
                    break;
                case 6:
                    presentation.deleteAnimal();
                    break;
                case 7:
                    presentation.showAllAnimals();
                    break;
                case 8:
                    presentation.moveAnimal();
                    break;
                case 9:
                    presentation.feedTortoise();
                    break;
                case 10:
                    presentation.printZoo();
                    break;
                case 11:
                    break;
                default:
                    System.out.println("Wrong answer my friend");
            }
            System.out.println("Whats next?");
            printMenu();
            task = scanner.nextInt();
        }
        presentation.saveData();

    }

    private void printMenu() {
        System.out.println("What would you like to do? Chose a number");
        System.out.println("1. Add owner");
        System.out.println("2. Delete owner");
        System.out.println("3. Check given owner animals");
        System.out.println("4. Check all animals and owners");
        System.out.println("5. Add animal");
        System.out.println("6. Delete animal");
        System.out.println("7. Show all animals");
        System.out.println("8. Move animal");
        System.out.println("9. Feed tortoise");
        System.out.println("10. Print zoo");
        System.out.println("11. Quite");
    }

}
