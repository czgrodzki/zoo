package pl.edu.pg.zoo.move;

import pl.edu.pg.zoo.animals.commons.Either;
import pl.edu.pg.zoo.animals.helpers.Position;

public class MoveResponse extends Either<String, Position> {

    public MoveResponse(boolean success, String left, Position right) {
        super(success, left, right);
    }

    public static MoveResponse success(Position right) {
        return new MoveResponse(true, null, right);
    }

    public static MoveResponse failure(String left) {
        return new MoveResponse(false, left, null);
    }
}
