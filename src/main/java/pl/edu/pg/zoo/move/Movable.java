package pl.edu.pg.zoo.move;

public interface Movable {

    MoveResponse move(MoveDirection direction);

}
