package pl.edu.pg.zoo.move;

public enum MoveDirection {

    UP, DOWN, LEFT, RIGHT
}
