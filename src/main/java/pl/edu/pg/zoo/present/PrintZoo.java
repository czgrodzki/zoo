package pl.edu.pg.zoo.present;

import pl.edu.pg.zoo.Constant;
import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;

import java.util.List;

public final class PrintZoo {

    public PrintZoo() {
        throw new UnsupportedOperationException();
    }

    public static void printZoo(List<Animal> animals) {
        int frameSize = Constant.ZOO_SIZE + 1;
        for (int j = 1; j <= frameSize + 1; j++) {
            System.out.print("-");
        }
        System.out.println();
        for (int i = 1; i <= frameSize; i++) {
            System.out.print("|");
            for (int j = 1; j <= frameSize; j++) {
                for (Animal animal: animals) {
                    if (animal.getPosition().getRow() == i && animal.getPosition().getColumn() == j){
                        if (animal instanceof Cat) {
                            System.out.print("C");
                            j++;
                        } else if (animal instanceof Dog) {
                            System.out.print("D");
                            j++;
                        } else if (animal instanceof Tortoise) {
                            System.out.print("T");
                            j++;
                        }
                    }
                }
                System.out.print(" ");
            }
                System.out.println("|");
        }
        for (int j = 1; j <= frameSize + 1; j++) {
            System.out.print("-");
        }
        System.out.println();
    }
}
