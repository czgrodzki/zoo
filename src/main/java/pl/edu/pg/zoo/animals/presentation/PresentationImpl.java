package pl.edu.pg.zoo.animals.presentation;

import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.animal.service.AnimalServiceImpl;
import pl.edu.pg.zoo.animals.animal.service.portal.AnimalService;
import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.cat.service.CatsServiceImpl;
import pl.edu.pg.zoo.animals.cat.service.portal.CatsService;
import pl.edu.pg.zoo.animals.cat.storage.CatsRepositoryImpl;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;
import pl.edu.pg.zoo.animals.commons.AnimalResponse;
import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.dog.service.DogsServiceImpl;
import pl.edu.pg.zoo.animals.dog.service.portal.DogsService;
import pl.edu.pg.zoo.animals.dog.storage.DogsRepositoryImpl;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;
import pl.edu.pg.zoo.animals.turtles.service.TurtlesServiceImpl;
import pl.edu.pg.zoo.animals.turtles.service.portal.TurtlesService;
import pl.edu.pg.zoo.animals.turtles.service.portal.TurtlesService.TortoiseRespond;
import pl.edu.pg.zoo.animals.turtles.storage.TurtlesRepositoryImpl;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;
import pl.edu.pg.zoo.move.MoveDirection;
import pl.edu.pg.zoo.move.MoveResponse;
import pl.edu.pg.zoo.owners.model.Owner;
import pl.edu.pg.zoo.owners.service.OwnerServiceImpl;
import pl.edu.pg.zoo.owners.service.port.OwnerService;
import pl.edu.pg.zoo.owners.storage.OwnersRepositoryImpl;
import pl.edu.pg.zoo.owners.storage.port.OwnersRepository;
import pl.edu.pg.zoo.present.PrintZoo;

import java.time.LocalDate;
import java.util.List;
import java.util.Scanner;

public class PresentationImpl implements AnimalPresentation, OwnerPresentation {

    CatsRepository catsRepository = new CatsRepositoryImpl();
    DogsRepository dogsRepository = new DogsRepositoryImpl();
    TurtlesRepository turtlesRepository = new TurtlesRepositoryImpl();
    OwnersRepository ownersRepository = new OwnersRepositoryImpl();
    OwnerService ownerService = new OwnerServiceImpl(ownersRepository, catsRepository, dogsRepository, turtlesRepository);
    CatsService catsService = new CatsServiceImpl(catsRepository);
    DogsService dogsService = new DogsServiceImpl(dogsRepository);
    TurtlesService turtlesService = new TurtlesServiceImpl(turtlesRepository);
    AnimalService animalService = new AnimalServiceImpl(catsRepository, dogsRepository, turtlesRepository);

    Scanner scanner = new Scanner(System.in);


    @Override
    public void addAnimal() {
        System.out.println("Remember to register owner first, cause you will need owner ID to register an animal");
        System.out.println("Which animal would you like to register? cat, dog, tortoise");
        String s = scanner.nextLine();
        System.out.println();
        switch (s) {
            case "cat":
                registerCat();
                break;
            case "dog":
                registerDog();
                break;
            case "tortoise":
                registerTortoise();
                break;
            default:
                System.out.println("Wrong type of animal, try again");
        }

    }

    private void registerCat() {
        System.out.println("Type name");
        String name = scanner.nextLine();
        System.out.println("Chose gender: MALE/FEMALE");
        String gender = scanner.nextLine();
        try {
            Gender.valueOf(gender);
        } catch (IllegalArgumentException ie) {
            System.out.println("Needs to be MALE or FEMALE");
            return;
        }
        Gender gender1 = Gender.valueOf(gender);
        System.out.println("Type year of birth");
        int year = scanner.nextInt();
        System.out.println("Type month of birth like 1-12");
        int month = scanner.nextInt();
        System.out.println("Type day of birth remember of correct number of days in month");
        int day = scanner.nextInt();
        System.out.println("Type owner id, must be correct! I will check that");
        long ownerId = scanner.nextLong();
        OwnerService.OwnerResponse ownerById = ownerService.findOwnerById(ownerId);
        if (!ownerById.isSuccess()) {
            System.out.println(ownerById.getLeft());
            return;
        }
        System.out.println("Type breed");
        String breed = scanner.nextLine();
        Cat cat = new Cat(name, gender1, LocalDate.of(year, month, day), ownerId, breed);
        catsService.addCat(cat);
        AnimalResponse catById = catsService.findCatById(cat.getId());
        if (catById.isSuccess()) {
            System.out.println(catById.getRight().getOwnerId() + " " + catById.getRight().getName());
        }

    }
    private void registerDog() {
        System.out.println("Type name");
        String name = scanner.nextLine();
        System.out.println("Chose gender: MALE/FEMALE");
        String gender = scanner.nextLine();
        try {
            Gender.valueOf(gender);
        } catch (IllegalArgumentException ie) {
            System.out.println("Needs to be MALE or FEMALE");
            return;
        }
        Gender gender1 = Gender.valueOf(gender);
        System.out.println("Type year of birth");
        int year = scanner.nextInt();
        System.out.println("Type month of birth like 1-12");
        int month = scanner.nextInt();
        System.out.println("Type day of birth remember of correct number of days in month");
        int day = scanner.nextInt();
        System.out.println("Type owner id, must be correct! I will check that");
        long ownerId = scanner.nextLong();
        OwnerService.OwnerResponse ownerById = ownerService.findOwnerById(ownerId);
        if (!ownerById.isSuccess()) {
            System.out.println(ownerById.getLeft());
            return;
        }
        System.out.println("Type breed");
        String breed = scanner.nextLine();
        Dog dog = new Dog(name, gender1, LocalDate.of(year, month, day), ownerId, breed);
        dogsService.addDog(dog);
        AnimalResponse dogById = dogsService.findDogById(dog.getId());
        if (dogById.isSuccess()) {
            System.out.println(dogById.getRight().getId() + " " + dogById.getRight().getName());
        }

    }

    private void registerTortoise() {
        System.out.println("Type name");
        String name = scanner.nextLine();
        System.out.println("Chose gender: MALE/FEMALE");
        String gender = scanner.nextLine();
        try {
            Gender.valueOf(gender);
        } catch (IllegalArgumentException ie) {
            System.out.println("Needs to be MALE or FEMALE");
            return;
        }
        Gender gender1 = Gender.valueOf(gender);
        System.out.println("Type year of birth");
        int year = scanner.nextInt();
        System.out.println("Type month of birth like 1-12");
        int month = scanner.nextInt();
        System.out.println("Type day of birth remember of correct number of days in month");
        int day = scanner.nextInt();
        System.out.println("Type owner id, must be correct! I will check that");
        long ownerId = scanner.nextLong();
        OwnerService.OwnerResponse ownerById = ownerService.findOwnerById(ownerId);
        if (!ownerById.isSuccess()) {
            System.out.println(ownerById.getLeft());
            return;
        }
        Tortoise tortoise = new Tortoise(name, gender1, LocalDate.of(year, month, day), ownerId);
        turtlesService.addTortoise(tortoise);
        TortoiseRespond tortoiseById = turtlesService.findTortoiseById(tortoise.getId());
        if (tortoiseById.isSuccess()) {
            System.out.println(tortoiseById.getRight().getId() + " " + tortoiseById.getRight().getName());
        }

    }

    @Override
    public void deleteAnimal() {
        System.out.println("Which animal would you like to delete?");
        String s = scanner.nextLine();
        switch (s) {
            case "cat":
                deleteCat();
                break;
            case "dog":
                deleteDog();
                break;
            case "tortoise":
                deleteTortoise();
                break;
            default:
                System.out.println("Wrong type of animal, try again");
        }
    }

    private void deleteCat() {
        System.out.println("Get id of animal that you would like to delete");
        catsService.findCats().forEach(Animal::getInfo);
        System.out.println("Type id");
        long catId = scanner.nextLong();
        catsService.deleteCatById(catId);

    }

    private void deleteDog() {
        System.out.println("Get id of animal that you would like to delete");
        dogsService.findDogs().forEach(Animal::getInfo);
        System.out.println("Type id");
        long dogId = scanner.nextLong();
        dogsService.deleteDogById(dogId);

    }

    private void deleteTortoise() {
        System.out.println("Get id of animal that you would like to delete");
        turtlesService.findTurtles().forEach(Animal::getInfo);
        System.out.println("Type id");
        long tortoiseId = scanner.nextLong();
        turtlesService.deleteTortoiseById(tortoiseId);

    }

    @Override
    public void changeAnimalName() {

    }

    @Override
    public void showAllAnimals() {
        animalService.getAnimals().forEach(Animal::getInfo);
    }

    @Override
    public void feedTortoise() {
        System.out.println("Get id of tortoise that you would like to feed");
        long tortoiseId = scanner.nextLong();
        TortoiseRespond tortoiseById = turtlesService.findTortoiseById(tortoiseId);
        if (!tortoiseById.isSuccess()) {
            System.out.println(tortoiseById.getLeft());
            return;
        }
        tortoiseById.getRight().feedTortoise();
    }

    @Override
    public void moveAnimal() {
        System.out.println("Which animal would you like to move? cat, dog, tortoise");
        String s = scanner.nextLine();
        switch (s) {
            case "cat":
                moveCat();
                break;
            case "dog":
                moveDog();
                break;
            case "tortoise":
                moveTortoise();
                break;
            default:
                System.out.println("Wrong type of animal, try again");
        }

    }

    private void moveCat() {
        System.out.println("Chose cat's id");
        catsService.findCats().forEach(Animal::getInfo);
        long catId = scanner.nextLong();
        scanner.skip("\n");
        AnimalResponse catById = catsService.findCatById(catId);
        if (!catById.isSuccess()) {
            System.out.println(catById.getLeft());
            return;
        }
        System.out.println("Chose direction: UP/DOWN/LEFT/RIGHT");
        String direction = scanner.nextLine();
        MoveResponse move = catById.getRight().move(MoveDirection.valueOf(direction));
        if (!move.isSuccess()) {
            System.out.println(move.getLeft());
            return;
        }
        System.out.println("Moved to " + move.getRight());
    }

    private void moveDog() {
        System.out.println("Chose dog's id");
        dogsService.findDogs().forEach(Animal::getInfo);
        long dogId = scanner.nextLong();
        scanner.skip("\n");
        AnimalResponse dogById = dogsService.findDogById(dogId);
        if (!dogById.isSuccess()) {
            System.out.println(dogById.getLeft());
            return;
        }
        System.out.println("Chose direction: UP/DOWN/LEFT/RIGHT");
        String direction = scanner.nextLine();
        MoveResponse move = dogById.getRight().move(MoveDirection.valueOf(direction));
        if (!move.isSuccess()) {
            System.out.println(move.getLeft());
            return;
        }
        System.out.println("Moved to " + move.getRight());

    }

    private void moveTortoise() {
        System.out.println("Chose tortoise's id");
        turtlesService.findTurtles().forEach(Animal::getInfo);
        long tortoiseId = scanner.nextLong();
        scanner.skip("\n");
        TortoiseRespond tortoiseById = turtlesService.findTortoiseById(tortoiseId);
        if (!tortoiseById.isSuccess()) {
            System.out.println(tortoiseById.getLeft());
            return;
        }
        System.out.println("Chose direction: UP/DOWN/LEFT/RIGHT");
        String direction = scanner.nextLine();
        MoveResponse move = tortoiseById.getRight().move(MoveDirection.valueOf(direction));
        if (!move.isSuccess()) {
            System.out.println(move.getLeft());
            return;
        }
        System.out.println("Moved to " + move.getRight());


    }

    @Override
    public void addAnimalOwner() {
        System.out.println("Which animal would you like to add owner to? cat, dog, tortoise");
        String s = scanner.nextLine();
        switch (s) {
            case "cat":
                addCatOwner();
                break;
            case "dog":
                addDogOwner();
                break;
            case "tortoise":
                addTortoiseOwner();
                break;
            default:
                System.out.println("Wrong type of animal, try again");
        }
    }

    private void addCatOwner() {
        System.out.println("Chose cat's id");
        catsService.findCats().forEach(Animal::getInfo);;
        long catId = scanner.nextLong();
        AnimalResponse catById = catsService.findCatById(catId);
        if (!catById.isSuccess()) {
            System.out.println(catById.getLeft());
            return;
        }
        System.out.println("Chose owner id ");
        ownerService.findOwners().forEach(System.out::println);
        long ownerId = scanner.nextLong();
        OwnerService.OwnerResponse ownerById = ownerService.findOwnerById(ownerId);
        if (!ownerById.isSuccess()) {
            System.out.println(ownerById.getLeft());
            return;
        }
        catById.getRight().setOwnerId(ownerById.getRight().getId());
    }

    private void addDogOwner() {
        System.out.println("Chose dog's id");
        dogsService.findDogs().forEach(Animal::getInfo);
        long dogId = scanner.nextLong();
        AnimalResponse dogById = dogsService.findDogById(dogId);
        if (!dogById.isSuccess()) {
            System.out.println(dogById.getLeft());
            return;
        }
        System.out.println("Chose owner id ");
        ownerService.findOwners().forEach(System.out::println);
        long ownerId = scanner.nextLong();
        OwnerService.OwnerResponse ownerById = ownerService.findOwnerById(ownerId);
        if (!ownerById.isSuccess()) {
            System.out.println(ownerById.getLeft());
            return;
        }
        dogById.getRight().setOwnerId(ownerById.getRight().getId());
    }

    private void addTortoiseOwner() {
        System.out.println("Chose tortoise's id");
        turtlesService.findTurtles().forEach(Animal::getInfo);
        long tortoiseId = scanner.nextLong();
        TortoiseRespond tortoiseById = turtlesService.findTortoiseById(tortoiseId);
        if (!tortoiseById.isSuccess()) {
            System.out.println(tortoiseById.getLeft());
            return;
        }
        System.out.println("Chose owner id ");
        ownerService.findOwners().forEach(System.out::println);
        long ownerId = scanner.nextLong();
        OwnerService.OwnerResponse ownerById = ownerService.findOwnerById(ownerId);
        if (!ownerById.isSuccess()) {
            System.out.println(ownerById.getLeft());
            return;
        }
        tortoiseById.getRight().setOwnerId(ownerById.getRight().getId());
    }

    @Override
    public void addOwner() {
        System.out.println("Type owner name");
        String name = scanner.nextLine();
        System.out.println("Type owner surname");
        String surname = scanner.nextLine();
        System.out.println("Type owner gender: MALE/FEMALE");
        String gender = scanner.nextLine();
        try {
            Gender.valueOf(gender);
        } catch (IllegalArgumentException ie) {
            System.out.println("Needs to be MALE or FEMALE");
            return;
        }

        System.out.println("Type year of birth");
        int year = scanner.nextInt();
        System.out.println("Type month of birth like 1-12");
        int month = scanner.nextInt();
        System.out.println("Type day of birth remember of correct number of days in month");
        int day = scanner.nextInt();
        Owner owner = new Owner(name, surname, Gender.valueOf(gender), LocalDate.of(year, month, day));
        OwnerService.OwnerResponse ownerResponse = ownerService.addOwner(owner);
        System.out.println(ownerResponse.getRight().getId()  + " " + ownerResponse.getRight().getName());
    }

    @Override
    public void deleteOwner() {
        System.out.println("Choose id of owner you would like to delete");
        ownerService.findOwners().forEach(System.out::println);
        long ownerId = scanner.nextLong();
        ownerService.deleteOwnerById(ownerId);
    }

    @Override
    public void showAllOwnersAndAnimals() {
        List<Owner> owners = ownerService.findOwners();
        for (Owner owner : owners) {
            System.out.println(owner);
            animalService.getAnimals()
                    .stream()
                    .forEach(animal -> {
                        if (animal.getOwnerId() == owner.getId()) {
                            animal.getInfo();
                            System.out.println();
                        }
                    });
        }
    }

    @Override
    public void showSingleOwnerAnimals() {
        System.out.println("Choose id of owner to see his/her animals");
        ownerService.findOwners().forEach(System.out::println);
        long ownerId = scanner.nextLong();
        OwnerService.OwnerResponse ownerById = ownerService.findOwnerById(ownerId);
        if (!ownerById.isSuccess()) {
            System.out.println(ownerById.getLeft());
            return;
        }
        animalService.getAnimals()
                .stream()
                .forEach(animal -> {
                    if (animal.getOwnerId() == ownerById.getRight().getId()) {
                        animal.getInfo();
                    }
                });
    }

    public void printZoo() {
        PrintZoo.printZoo(animalService.getAnimals());
    }

    public void saveData() {
        animalService.saveData();
        ownerService.saveData();
    }

    public void loadData() {
        animalService.loadData();
        ownerService.loadData();
    }
}
