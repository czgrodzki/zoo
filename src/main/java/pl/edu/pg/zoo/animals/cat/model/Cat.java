package pl.edu.pg.zoo.animals.cat.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.edu.pg.zoo.animals.mammal.model.Mammal;
import pl.edu.pg.zoo.animals.helpers.Gender;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@ToString
@NoArgsConstructor
public class Cat extends Mammal implements Serializable {

    private static long counter = 0;

    public Cat(String name, Gender gender, LocalDate birthDate, long ownerId, String breed) {
        super(counter++, name, gender, birthDate, ownerId, breed);
    }

}
