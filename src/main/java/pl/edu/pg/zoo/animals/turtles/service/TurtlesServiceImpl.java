package pl.edu.pg.zoo.animals.turtles.service;

import pl.edu.pg.zoo.animals.commons.AnimalResponse;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;
import pl.edu.pg.zoo.animals.turtles.service.portal.TurtlesService;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class TurtlesServiceImpl implements TurtlesService {

    private final TurtlesRepository repository;

    public TurtlesServiceImpl(TurtlesRepository repository) {
        this.repository = repository;
    }

    @Override
    public TortoiseRespond addTortoise(Tortoise tortoise) {
        if (tortoise.getBirthDate().isAfter(LocalDate.now())
                || tortoise.getBirthDate().isBefore(LocalDate.of(1900, 1, 1))) {
            return TortoiseRespond.failure("Wrong birth date");
        }
        repository.addTortoise(tortoise);
        return TortoiseRespond.success(tortoise);
    }

    @Override
    public TortoiseRespond findTortoiseById(long id) {
        Optional<Tortoise> tortoise = repository.findTortoiseById(id);
        return tortoise.map(TortoiseRespond::success)
                .orElseGet(() -> TortoiseRespond.failure("There is no tortoise with given id"));
    }

    @Override
    public void deleteTortoiseById(long id) {
        TortoiseRespond lookForTortoiseResult = findTortoiseById(id);
        if (lookForTortoiseResult.isSuccess()) {
            repository.deleteTortoiseById(id);
        }
    }

    @Override
    public List<Tortoise> findTurtles() {
        return repository.findTurtles();
    }

    @Override
    public TortoiseRespond updateTortoisesOwner(long tortoiseId, long ownerId) {
        Optional<Tortoise> tortoise = repository.updateTortoisesOwner(tortoiseId, ownerId);
        return tortoise.map(TortoiseRespond::success)
                .orElseGet(() -> TortoiseRespond.failure("There is no tortoise with given id"));
    }

}
