package pl.edu.pg.zoo.animals.animal.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pg.zoo.Constant;
import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.animal.service.portal.AnimalService;
import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;
import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class AnimalServiceImpl implements AnimalService {

    private static final Logger log = LoggerFactory.getLogger(AnimalServiceImpl.class);

    private final CatsRepository catsRepository;
    private final DogsRepository dogsRepository;
    private final TurtlesRepository turtlesRepository;

    public AnimalServiceImpl(CatsRepository catsRepository, DogsRepository dogsRepository, TurtlesRepository turtlesRepository) {
        this.catsRepository = catsRepository;
        this.dogsRepository = dogsRepository;
        this.turtlesRepository = turtlesRepository;
    }


    @Override
    public void saveData() {
        try (FileOutputStream f = new FileOutputStream((Constant.ANIMALS_FILE));
             ObjectOutputStream o = new ObjectOutputStream(f)) {
            o.writeObject(getAnimals());
        } catch (IOException e) {
            log.error("Error saving cats ", e);
            e.printStackTrace();
        }

    }

    @Override
    public void loadData() {
        try (FileInputStream fi = new FileInputStream(Constant.ANIMALS_FILE);
             ObjectInputStream oi = new ObjectInputStream(fi)) {
                List<Animal> animals = (List<Animal>) oi.readObject();
                animals.forEach(animal -> {
                    if (animal instanceof Cat) {
                        catsRepository.addCat((Cat) animal);
                    } else if (animal instanceof Dog) {
                        dogsRepository.addDog((Dog) animal);
                    } else {
                        turtlesRepository.addTortoise((Tortoise) animal);
                    }
                });

        } catch (IOException e) {
            log.error("Error loading cats", e);
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Animal> getAnimals() {
        List<Animal> animals = new ArrayList<>();
        animals.addAll(catsRepository.findCats());
        animals.addAll(dogsRepository.findDogs());
        animals.addAll(turtlesRepository.findTurtles());
        return animals;
    }

}
