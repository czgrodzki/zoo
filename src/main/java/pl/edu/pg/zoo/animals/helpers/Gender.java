package pl.edu.pg.zoo.animals.helpers;

import java.io.Serializable;

public enum Gender implements Serializable {

    FEMALE, MALE

}
