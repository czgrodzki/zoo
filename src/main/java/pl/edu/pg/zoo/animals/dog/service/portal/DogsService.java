package pl.edu.pg.zoo.animals.dog.service.portal;

import pl.edu.pg.zoo.animals.commons.AnimalResponse;
import pl.edu.pg.zoo.animals.dog.model.Dog;

import java.util.List;

public interface DogsService {

    AnimalResponse addDog(Dog dog);
    AnimalResponse findDogById(long id);
    void deleteDogById(long id);
    List<Dog> findDogs();
    AnimalResponse updateDogsOwner(long id, long ownerId);

}
