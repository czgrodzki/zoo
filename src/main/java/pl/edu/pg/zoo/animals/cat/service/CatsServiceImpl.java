package pl.edu.pg.zoo.animals.cat.service;

import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.cat.service.portal.CatsService;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;
import pl.edu.pg.zoo.animals.commons.AnimalResponse;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class CatsServiceImpl implements CatsService {

    private final CatsRepository repository;

    public CatsServiceImpl(CatsRepository repository) {
        this.repository = repository;
    }

    @Override
    public AnimalResponse addCat(Cat cat) {
        if (cat.getBirthDate().isAfter(LocalDate.now())
                || cat.getBirthDate().isBefore(LocalDate.of(1900, 1, 1))) {
            return AnimalResponse.failure("Wrong birth date");
        }
        repository.addCat(cat);
        return AnimalResponse.success(cat);
    }

    @Override
    public AnimalResponse findCatById(long id) {
        Optional<Cat> cat = repository.findCatById(id);
        return cat.map(AnimalResponse::success)
                .orElseGet(() -> AnimalResponse.failure("There is no cat with given id"));
    }

    @Override
    public void deleteCatById(long id) {
        AnimalResponse lookForCatResult = findCatById(id);
        if (lookForCatResult.isSuccess()) {
            repository.deleteCatById(id);
        }
    }

    @Override
    public List<Cat> findCats() {
        return repository.findCats();
    }

    @Override
    public AnimalResponse updateCatsOwner(long id, long ownerId) {
        Optional<Cat> cat = repository.updateCatsOwner(id, ownerId);
        return cat.map(AnimalResponse::success)
                .orElseGet(() -> AnimalResponse.failure("There is no cat with given id"));
    }

}
