package pl.edu.pg.zoo.animals.commons;

import pl.edu.pg.zoo.animals.animal.model.Animal;

public class AnimalResponse extends Either<String, Animal> {

    public AnimalResponse(boolean success, String left, Animal right) {
        super(success, left, right);
    }

    public static AnimalResponse success(Animal right) {
        return new AnimalResponse(true, null, right);
    }

    public static AnimalResponse failure(String left) {
        return new AnimalResponse(false, left, null);
    }

}
