package pl.edu.pg.zoo.animals.turtles.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.edu.pg.zoo.Constant;
import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.move.MoveDirection;
import pl.edu.pg.zoo.move.MoveResponse;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@ToString
public class Tortoise extends Animal implements Serializable {

    private static long counter = 0;

    private boolean isActive;

    public Tortoise(String name, Gender gender, LocalDate birthDate, long ownerId) {
        super(counter++, name, gender, birthDate, ownerId);
        this.isActive = true;
    }

    public boolean feedTortoise() {
        return isActive = true;
    }

    @Override
    public MoveResponse move(MoveDirection direction) {
        if (!isActive) {
            return new MoveResponse(false, "Tortoise is hidden in shell", null);
        }
        isActive=false;

        int row = position.getRow();
        int column = position.getColumn();
        MoveResponse movedSuccessfully = new MoveResponse(true, null, position);

        switch (direction) {
            case UP:
                if (row - 1 >= 1) {
                    position.setRow(row + 1);
                    return movedSuccessfully;
                }
                break;
            case DOWN:
                if (row + 1 <= Constant.ZOO_SIZE) {
                    position.setRow(row + 1);
                    return movedSuccessfully;
                }
                break;
            case RIGHT:
                if (column + 1 <= Constant.ZOO_SIZE) {
                    position.setColumn(column + 1);
                    return movedSuccessfully;
                }
                break;
            case LEFT:
                if (column - 1 >= 1) {
                    position.setColumn(column - 1);
                    return movedSuccessfully;
                }
                break;
        }
        return new MoveResponse(false, "Animal is standing next to the fence", null);
    }

}
