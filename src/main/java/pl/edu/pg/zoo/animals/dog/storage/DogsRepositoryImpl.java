package pl.edu.pg.zoo.animals.dog.storage;

import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class DogsRepositoryImpl implements DogsRepository {

    private final List<Dog> dogs = new ArrayList<>();

    @Override
    public void addDog(Dog dog) {
        dogs.add(dog);
    }

    @Override
    public Optional<Dog> findDogById(long id) {
        return dogs.stream()
                .filter(dog -> dog.getId() == id)
                .findFirst();
    }

    @Override
    public void deleteDogById(long id) {
        dogs.removeIf(dog -> dog.getId() == id);
    }

    @Override
    public List<Dog> findDogs() {
        return dogs;
    }

    @Override
    public Optional<Dog> updateDogsOwner(long dogId, long ownerId) {
        Optional<Dog> dog = findDogById(dogId);
        if (dog.isPresent()) {
            dog.get().setOwnerId(ownerId);
            return dog;
        }
        return Optional.empty();
    }

}
