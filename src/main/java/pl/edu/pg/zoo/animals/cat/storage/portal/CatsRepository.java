package pl.edu.pg.zoo.animals.cat.storage.portal;

import pl.edu.pg.zoo.animals.cat.model.Cat;

import java.util.List;
import java.util.Optional;

public interface CatsRepository {

    void addCat(Cat cat);
    Optional<Cat> findCatById(long id);
    void deleteCatById(long id);
    List<Cat> findCats();
    Optional<Cat> updateCatsOwner(long id, long ownerId);

}
