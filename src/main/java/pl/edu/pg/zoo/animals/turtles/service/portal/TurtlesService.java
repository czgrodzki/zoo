package pl.edu.pg.zoo.animals.turtles.service.portal;

import pl.edu.pg.zoo.animals.commons.Either;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;

import java.util.List;

public interface TurtlesService {

    TortoiseRespond addTortoise(Tortoise tortoise);
    TortoiseRespond findTortoiseById(long id);
    void deleteTortoiseById(long id);
    List<Tortoise> findTurtles();
    TortoiseRespond updateTortoisesOwner(long id, long ownerId);

     class TortoiseRespond extends Either<String, Tortoise> {

        public TortoiseRespond(boolean success, String left, Tortoise right) {
            super(success, left, right);
        }

        public static TortoiseRespond success(Tortoise right) {
            return new TortoiseRespond(true, null, right);
        }

        public static TortoiseRespond failure(String left) {
            return new TortoiseRespond(false, left, null);
        }

    }

}
