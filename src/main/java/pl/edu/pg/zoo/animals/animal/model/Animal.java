package pl.edu.pg.zoo.animals.animal.model;

import lombok.*;
import pl.edu.pg.zoo.Constant;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.animals.helpers.Position;
import pl.edu.pg.zoo.move.Movable;
import pl.edu.pg.zoo.move.MoveDirection;
import pl.edu.pg.zoo.move.MoveResponse;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;

@Data
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Animal implements Movable, Serializable {

    protected long id = 0;
    protected String name;
    protected Gender gender;
    protected LocalDate birthDate;
    protected Position position;
    protected long ownerId;

    public Animal(long id, String name, Gender gender, LocalDate birthDate, long ownerId) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.birthDate = birthDate;
        this.ownerId = ownerId;
        this.position = new Position();
    }

    public int getAge() {
        return Period.between(LocalDate.now(), birthDate).getYears();
    }

    public void getInfo() {
        System.out.print(name);
        System.out.print(" " + gender);
        System.out.print(" " + birthDate);
        System.out.print(" " + ownerId);
        System.out.print(" " + position);
    }


    @Override
    public MoveResponse move(MoveDirection direction) {
        int row = position.getRow();
        int column = position.getColumn();

        MoveResponse movedSuccessfully = new MoveResponse(true, null, position);
        MoveResponse cannotBeMoved = new MoveResponse(false, "Animal is standing next to the fence", null);

        switch (direction) {
            case UP:
                if (row - 1 >= 1) {
                    position.setRow(row + 1);
                    return movedSuccessfully;
                }
                break;
            case DOWN:
                if (row + 1 <= Constant.ZOO_SIZE) {
                    position.setRow(row + 1);
                    return movedSuccessfully;
                }
                break;
            case RIGHT:
                if (column + 1 <= Constant.ZOO_SIZE) {
                    position.setColumn(column + 1);
                    return movedSuccessfully;
                }
                break;
            case LEFT:
                if (column - 1 >= 1) {
                    position.setColumn(column - 1);
                    return movedSuccessfully;
                }
                break;
        }
        return cannotBeMoved;
    }

}
