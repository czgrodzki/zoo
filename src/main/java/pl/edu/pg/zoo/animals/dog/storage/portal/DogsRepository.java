package pl.edu.pg.zoo.animals.dog.storage.portal;

import pl.edu.pg.zoo.animals.dog.model.Dog;

import java.util.List;
import java.util.Optional;

public interface DogsRepository {

    void addDog(Dog dog);
    Optional<Dog> findDogById(long id);
    void deleteDogById(long id);
    List<Dog> findDogs();
    Optional<Dog> updateDogsOwner(long dogId, long ownerId);

}
