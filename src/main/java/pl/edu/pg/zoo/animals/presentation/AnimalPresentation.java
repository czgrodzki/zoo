package pl.edu.pg.zoo.animals.presentation;

public interface AnimalPresentation {

    void addAnimal();
    void deleteAnimal();
    void changeAnimalName();
    void showAllAnimals();
    void feedTortoise();
    void moveAnimal();
    void addAnimalOwner();

}
