package pl.edu.pg.zoo.animals.turtles.storage.portal;

import pl.edu.pg.zoo.animals.turtles.model.Tortoise;

import java.util.List;
import java.util.Optional;

public interface TurtlesRepository {

    void addTortoise(Tortoise tortoise);
    Optional<Tortoise> findTortoiseById(long id);
    void deleteTortoiseById(long id);
    List<Tortoise> findTurtles();
    Optional<Tortoise> updateTortoisesOwner(long id, long ownerId);

}
