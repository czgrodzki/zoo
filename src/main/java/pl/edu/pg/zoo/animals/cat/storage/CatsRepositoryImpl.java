package pl.edu.pg.zoo.animals.cat.storage;

import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CatsRepositoryImpl implements CatsRepository {

    private final List<Cat> cats = new ArrayList<>();

    @Override
    public void addCat(Cat cat) {
        cats.add(cat);
    }

    @Override
    public Optional<Cat> findCatById(long id) {
        return cats.stream()
                .filter(cat -> cat.getId() == id)
                .findFirst();
    }

    @Override
    public void deleteCatById(long id) {
        cats.removeIf(cat -> cat.getId() == id);
    }

    @Override
    public List<Cat> findCats() {
        return cats;
    }

    @Override
    public Optional<Cat> updateCatsOwner(long catId, long ownerId) {
        Optional<Cat> cat = findCatById(catId);
        if (cat.isPresent()) {
            cat.get().setOwnerId(ownerId);
            return cat;
        }
        return Optional.empty();
    }

}
