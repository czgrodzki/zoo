package pl.edu.pg.zoo.animals.turtles.storage;

import pl.edu.pg.zoo.animals.turtles.model.Tortoise;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TurtlesRepositoryImpl implements TurtlesRepository {

    private final List<Tortoise> turtles = new ArrayList<>();

    @Override
    public void addTortoise(Tortoise tortoise) {
        turtles.add(tortoise);
    }

    @Override
    public Optional<Tortoise> findTortoiseById(long id) {
        return turtles.stream()
                .filter(tortoise -> tortoise.getId() == id)
                .findFirst();
    }

    @Override
    public void deleteTortoiseById(long id) {
        turtles.removeIf(tortoise -> tortoise.getId() == id);
    }

    @Override
    public List<Tortoise> findTurtles() {
        return turtles;
    }

    @Override
    public Optional<Tortoise> updateTortoisesOwner(long id, long ownerId) {
        Optional<Tortoise> tortoise = findTortoiseById(id);
        if (tortoise.isPresent()) {
            tortoise.get().setOwnerId(ownerId);
            return tortoise;
        }
        return Optional.empty();
    }

}
