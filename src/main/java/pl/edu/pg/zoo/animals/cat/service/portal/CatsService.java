package pl.edu.pg.zoo.animals.cat.service.portal;

import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.commons.AnimalResponse;

import java.util.List;

public interface CatsService {

    AnimalResponse addCat(Cat cat);
    AnimalResponse findCatById(long id);
    void deleteCatById(long id);
    List<Cat> findCats();
    AnimalResponse updateCatsOwner(long id, long ownerId);

}
