package pl.edu.pg.zoo.animals.dog.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import pl.edu.pg.zoo.Constant;
import pl.edu.pg.zoo.animals.mammal.model.Mammal;
import pl.edu.pg.zoo.animals.helpers.Gender;
import pl.edu.pg.zoo.move.Movable;
import pl.edu.pg.zoo.move.MoveDirection;
import pl.edu.pg.zoo.move.MoveResponse;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@EqualsAndHashCode
@ToString
public class Dog extends Mammal implements Movable, Serializable {

    private static long counter = 0;

    public Dog(String name, Gender gender, LocalDate birthDate, long ownerId, String breed) {
        super(counter++, name, gender, birthDate, ownerId, breed);
    }


    @Override
    public MoveResponse move(MoveDirection direction) {
        int row = position.getRow();
        int column = position.getColumn();

        MoveResponse movedSuccessfully = new MoveResponse(true, null, position);
        MoveResponse cannotBeMoved = new MoveResponse(false, "Cat is standing next to the fence", null);

        switch (direction) {
            case UP:
                if (row - 1 >= 0) {
                    position.setRow(row+1);
                    return movedSuccessfully;
                } else {
                    return cannotBeMoved;
                }
            case DOWN:
                if (row + 1 <= Constant.ZOO_SIZE) {
                    position.setRow(row+1);
                    return movedSuccessfully;
                } else {
                    return cannotBeMoved;
                }
            case RIGHT:
                if (column + 1 <= Constant.ZOO_SIZE) {
                    position.setColumn(column + 1);
                    return movedSuccessfully;
                } else {
                    return cannotBeMoved;
                }
            case LEFT:
                if (column - 1 >= 0) {
                    position.setColumn(column - 1);
                    return movedSuccessfully;
                } else {
                    return cannotBeMoved;
                }
            default:
                return cannotBeMoved;
        }
    }
}
