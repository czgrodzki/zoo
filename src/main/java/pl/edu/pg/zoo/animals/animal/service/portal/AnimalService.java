package pl.edu.pg.zoo.animals.animal.service.portal;

import pl.edu.pg.zoo.Storable;
import pl.edu.pg.zoo.animals.animal.model.Animal;

import java.util.List;

public interface AnimalService extends Storable {

    List<Animal> getAnimals();

}
