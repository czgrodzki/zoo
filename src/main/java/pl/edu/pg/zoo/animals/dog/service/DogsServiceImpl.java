package pl.edu.pg.zoo.animals.dog.service;

import pl.edu.pg.zoo.animals.commons.AnimalResponse;
import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.dog.service.portal.DogsService;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class DogsServiceImpl implements DogsService {

    private final DogsRepository repository;

    public DogsServiceImpl(DogsRepository repository) {
        this.repository = repository;
    }

    @Override
    public AnimalResponse addDog(Dog dog) {
        if (dog.getBirthDate().isAfter(LocalDate.now())
                || dog.getBirthDate().isBefore(LocalDate.of(1900, 1, 1))) {
            return AnimalResponse.failure("Wrong birth date");
        }
        repository.addDog(dog);
        return AnimalResponse.success(dog);
    }

    @Override
    public AnimalResponse findDogById(long id) {
        Optional<Dog> dog = repository.findDogById(id);
        return dog.map(AnimalResponse::success)
                .orElseGet(() -> AnimalResponse.failure("There is no dog with given id"));
    }

    @Override
    public void deleteDogById(long id) {
        AnimalResponse lookForDogResult = findDogById(id);
        if (lookForDogResult.isSuccess()) {
            repository.deleteDogById(id);
        }
    }

    @Override
    public List<Dog> findDogs() {
        return repository.findDogs();
    }

    @Override
    public AnimalResponse updateDogsOwner(long dogsId, long ownerId) {
        Optional<Dog> dog = repository.updateDogsOwner(dogsId, ownerId);
        return dog.map(AnimalResponse::success)
                .orElseGet(() -> AnimalResponse.failure("There is no dog with given id"));
    }

}
