package pl.edu.pg.zoo.animals.helpers;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
public class Position implements Serializable {

    private int row;
    private int column;

    public Position() {
        this.row = 1;
        this.column = 1;
    }

}
