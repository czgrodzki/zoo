package pl.edu.pg.zoo.animals.presentation;

public interface OwnerPresentation {

    void addOwner();
    void deleteOwner();
    void showAllOwnersAndAnimals();
    void showSingleOwnerAnimals();

}
