package pl.edu.pg.zoo.animals.mammal.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.helpers.Gender;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@ToString
public class Mammal extends Animal implements Serializable {

    private String breed;

    public Mammal(long id, String name, Gender gender, LocalDate birthDate, long ownerId, String breed) {
        super(id, name, gender, birthDate, ownerId);
        this.breed = breed;
    }

}
