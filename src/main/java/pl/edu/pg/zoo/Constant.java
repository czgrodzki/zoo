package pl.edu.pg.zoo;

public final class Constant {

   public static final int ZOO_SIZE = 10;
   public static final String ANIMALS_FILE = "src/main/resources/animals.txt";
   public static final String OWNERS_FILE = "src/main/resources/owners.txt";

}
