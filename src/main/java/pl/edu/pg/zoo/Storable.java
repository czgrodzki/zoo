package pl.edu.pg.zoo;

public interface Storable {

    void saveData();
    void loadData();
}
