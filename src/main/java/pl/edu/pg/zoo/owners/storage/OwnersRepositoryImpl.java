package pl.edu.pg.zoo.owners.storage;

import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.owners.model.Owner;
import pl.edu.pg.zoo.owners.storage.port.OwnersRepository;

import java.util.*;

public class OwnersRepositoryImpl implements OwnersRepository {

    private final List<Owner> owners = new ArrayList<>();

    @Override
    public void addOwner(Owner owner) {
       owners.add(owner);
    }

    @Override
    public Optional<Owner> findOwnerById(long id) {
        return owners.stream()
                .filter(owner -> owner.getId() == id)
                .findFirst();
    }

    @Override
    public void deleteOwnerById(long id) {
        owners.removeIf(owner -> owner.getId() == id);
    }

    @Override
    public List<Owner> findOwners() {
        return owners;
    }

    @Override
    public Optional<Owner> updateOwner(long id, String name, String surname) {
        Optional<Owner> owner = findOwnerById(id);
        if (owner.isPresent()) {
            owner.get().setName(name);
            owner.get().setSurname(surname);
            return owner;
        }
        return Optional.empty();

    }

    @Override
    public void loadOwners(List<Owner> owners) {
        this.owners.clear();
        this.owners.addAll(owners);
    }
}
