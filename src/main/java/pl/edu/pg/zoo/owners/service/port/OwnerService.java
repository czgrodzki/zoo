package pl.edu.pg.zoo.owners.service.port;

import lombok.AllArgsConstructor;
import lombok.Data;
import pl.edu.pg.zoo.Storable;
import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.commons.Either;
import pl.edu.pg.zoo.owners.model.Owner;

import java.util.List;
import java.util.Map;

public interface OwnerService extends Storable {

    OwnerResponse addOwner(Owner owner);
    OwnerResponse findOwnerById(long id);
    void deleteOwnerById(long id);
    List<Owner> findOwners();
    OwnerResponse updateOwner(long id, String name, String surname);
    Map<Owner, List<Animal>> getOwnersWithTheirAnimals();
    void loadOwners(List<Owner> owners);

    @Data
    @AllArgsConstructor
    class UpdateOwnerCommand {
        private String name;
        private String surname;
    }

    class OwnerResponse extends Either<String, Owner> {

        public OwnerResponse(boolean success, String left, Owner right) {
            super(success, left, right);
        }

        public static OwnerResponse success(Owner right) {
            return new OwnerResponse(true, null, right);
        }

        public static OwnerResponse failure(String left) {
            return new OwnerResponse(false, left, null);
        }
    }

}
