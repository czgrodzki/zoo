package pl.edu.pg.zoo.owners.model;

import lombok.*;
import pl.edu.pg.zoo.animals.helpers.Gender;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;

@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Owner implements Serializable {

    private static long counter = 0;
    private long id;
    private String name;
    private String surname;
    private Gender gender;
    private LocalDate birthDate;

    public Owner(String name, String surname, Gender gender, LocalDate birthDate) {
        this.id = counter++;
        this.name = name;
        this.surname = surname;
        this.gender = gender;
        this.birthDate = birthDate;

    }

    public int getAge() {
        return Period.between(LocalDate.now(), birthDate).getYears();
    }

}
