package pl.edu.pg.zoo.owners.storage.port;

import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.owners.model.Owner;

import java.util.List;
import java.util.Optional;

public interface OwnersRepository {

    void addOwner(Owner owner);
    Optional<Owner> findOwnerById(long id);
    void deleteOwnerById(long id);
    List<Owner> findOwners();
    Optional<Owner> updateOwner(long id, String name, String surname);
    void loadOwners(List<Owner> owners);

}
