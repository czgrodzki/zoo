package pl.edu.pg.zoo.owners.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pg.zoo.Constant;
import pl.edu.pg.zoo.animals.animal.model.Animal;
import pl.edu.pg.zoo.animals.cat.model.Cat;
import pl.edu.pg.zoo.animals.cat.storage.portal.CatsRepository;
import pl.edu.pg.zoo.animals.dog.model.Dog;
import pl.edu.pg.zoo.animals.dog.storage.portal.DogsRepository;
import pl.edu.pg.zoo.animals.turtles.model.Tortoise;
import pl.edu.pg.zoo.animals.turtles.storage.portal.TurtlesRepository;
import pl.edu.pg.zoo.owners.model.Owner;
import pl.edu.pg.zoo.owners.storage.port.OwnersRepository;
import pl.edu.pg.zoo.owners.service.port.OwnerService;

import java.io.*;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class OwnerServiceImpl implements OwnerService {

    //private static final Logger log = LoggerFactory.getLogger(OwnerServiceImpl.class);

    private final OwnersRepository ownersRepository;
    private final CatsRepository catsRepository;
    private final DogsRepository dogsRepository;
    private final TurtlesRepository turtlesRepository;

    public OwnerServiceImpl(OwnersRepository repository, CatsRepository catsRepository, DogsRepository dogsRepository, TurtlesRepository turtlesRepository) {
        this.ownersRepository = repository;
        this.catsRepository = catsRepository;
        this.dogsRepository = dogsRepository;
        this.turtlesRepository = turtlesRepository;
    }

    @Override
    public OwnerResponse addOwner(Owner owner) {
        if (owner.getBirthDate().isAfter(LocalDate.now())
                || owner.getBirthDate().isBefore(LocalDate.of(1900, 1, 1))) {
            return OwnerResponse.failure("Wrong birth date");
        }
        ownersRepository.addOwner(owner);
        return OwnerResponse.success(owner);
    }

    @Override
    public OwnerResponse findOwnerById(long id) {
        Optional<Owner> owner = ownersRepository.findOwnerById(id);
        return owner.map(OwnerResponse::success)
                .orElseGet(() -> OwnerResponse.failure("There is no owner with given id"));
    }

    @Override
    public void deleteOwnerById(long id) {
        OwnerResponse lookForOwnerResult = findOwnerById(id);
        if (lookForOwnerResult.isSuccess()) {
            ownersRepository.deleteOwnerById(id);
            catsRepository.findCats()
                    .forEach(a -> {
                       if (a.getOwnerId() == id) {
                           catsRepository.deleteCatById(a.getId());
                       }
                    });
            dogsRepository.findDogs()
                    .forEach(a -> {
                        if (a.getOwnerId() == id) {
                            dogsRepository.deleteDogById(a.getId());
                        }
                    });
            turtlesRepository.findTurtles()
                    .forEach(a -> {
                        if (a.getOwnerId() == id) {
                            turtlesRepository.deleteTortoiseById(a.getId());
                        }
                    });
        }
    }

    @Override
    public List<Owner> findOwners() {
        return ownersRepository.findOwners();
    }

    @Override
    public OwnerResponse updateOwner(long id, String name, String surname) {
        Optional<Owner> owner = ownersRepository.updateOwner(id, name, surname);
        return owner.map(OwnerResponse::success)
                .orElseGet(() -> OwnerResponse.failure("There is no owner with given id"));
    }

    @Override
    public Map<Owner, List<Animal>> getOwnersWithTheirAnimals() {
        return ownersRepository.findOwners()
                .stream()
                .collect(Collectors.toMap(Function.identity(), owner -> getOwnersAnimals(owner.getId())));

    }

    @Override
    public void loadOwners(List<Owner> owners) {
        ownersRepository.loadOwners(owners);
    }


    private List<Animal> getOwnersAnimals(long ownerId) {
        return Stream.concat(
                        Stream.concat(dogsRepository.findDogs().stream(), catsRepository.findCats().stream()), turtlesRepository.findTurtles().stream())
                .filter(animal -> animal.getOwnerId() == ownerId)
                .collect(Collectors.toList());
    }

    @Override
    public void saveData() {
        try (FileOutputStream f = new FileOutputStream((Constant.OWNERS_FILE));
             ObjectOutputStream o = new ObjectOutputStream(f)) {
            o.writeObject(findOwners());
        } catch (FileNotFoundException e) {
            log.error("Error saving cats ", e);
            e.printStackTrace();
        } catch (IOException e) {
            log.error("Error saving cats ", e);
            e.printStackTrace();
        }

    }

    @Override
    public void loadData() {
        try (FileInputStream fi = new FileInputStream(Constant.OWNERS_FILE);
             ObjectInputStream oi = new ObjectInputStream(fi)) {

                List<Owner> owners = (List<Owner>) oi.readObject();
                loadOwners(owners);

        } catch (IOException e) {
            log.error("Error loading owners", e);
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
